<?php

/**
 * @file
 * Definition of variables for Variable API module.
 */

/**
 * Implements hook_variable_info().
 */
function bootstrap_site_alert_variable_info($options) {
  // bootstrap_site_alert defaults settings.
  $defaults = array(
    'group' => 'bootstrap_site_alert',
    'localize' => TRUE,
    'multidomain' => TRUE,
  );

  $variables['bootstrap_site_alert_active'] = array(
      'type' => 'boolean',
      'title' => t('If Checked, Bootstrap Site Alert is Active.'),
      'default' => variable_get('bootstrap_site_alert_active', FALSE),
    ) + $defaults;

  $variables['bootstrap_site_alert_severity'] = array(
      'type' => 'select',
      'title' => t('Severity'),
      'description' => t('Select the severity of the alert.'),
      'options' => array(
        'alert-success' => t('Success'),
        'alert-info' => t('Info'),
        'alert-warning' => t('Warning'),
        'alert-danger' => t('Danger'),
      ),
      'default' => variable_get('bootstrap_site_alert_severity', NULL),
    ) + $defaults;

  $variables['bootstrap_site_alert_dismiss'] = array(
      'type' => 'boolean',
      'title' => t('Make this alert dismissable?'),
      'default' => variable_get('bootstrap_site_alert_dismiss', FALSE),
    ) + $defaults;

  $variables['bootstrap_site_alert_no_admin'] = array(
      'type' => 'boolean',
      'title' => t('Hide this alert on admin pages?'),
      'default' => variable_get('bootstrap_site_alert_no_admin', FALSE),
    ) + $defaults;

  $message = variable_get('bootstrap_site_alert_message', array('value' => '', 'format' => NULL));

  $variables['bootstrap_site_alert_message'] = array(
      '#type' => 'textarea',
      '#title' => t('Alert Message'),
      'description' => t('Alert Message'),
      '#default_value' => $message['value'],
      'element' => array(
        '#rows' => 5,
      ),
    ) + $defaults;

  return $variables;
}

/**
 * Implements hook_variable_group_info().
 */
function bootstrap_site_alert_variable_group_info() {
  $groups['bootstrap_site_alert'] = array(
    'title' => t('Bootstrap Site Alert'),
    'description' => t('Configure bootstrap site alert.'),
    'access' => 'administer bootstrap site alerts',
    'path' => array('admin/config/system/bootstrap-site-alert'),
  );

  return $groups;
}
